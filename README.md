# README #

## Тема 1. Программирование теоретико-числовых алгоритмов.

### Написать процедуры для вычисления следующих теоретико-числовых функций:

☑	[1. Функция Эйлера.](https://bitbucket.org/MFIsmagilov/mathematical-support-of-information-security/raw/master/core/EulerFunction/)

☑	[2. Функция, вычисляющая порядок любого элемента конечного поля](https://bitbucket.org/MFIsmagilov/mathematical-support-of-information-security/raw/master/core/ElementOrder.py/)

☐	3. Программа для выполнения арифметических действий в конечном поле  

☐	4. Программа для выполнения арифметических действий в поле   (поле многочленов  по модулю неприводимого многочлена второй степени).

☑	[5. Функция проверки простоты натурального числа методом перебора.](https://bitbucket.org/MFIsmagilov/mathematical-support-of-information-security/raw/master/core/Brute.py/)

☐	6. Функция проверки, является ли данное число   свидетелем простоты натурального числа

☑	[7. Функция проверки простоты натурального числа алгоритмом Миллера-Рабина.](https://bitbucket.org/MFIsmagilov/mathematical-support-of-information-security/raw/master/core/MillerRabin.py/)
