from PyQt5.QtCore import QRegExp
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtGui import QRegExpValidator
from PyQt5.QtWidgets import *
from PyQt5.uic import loadUi

import re

from core.dimons.ExtendedGaluaField import *
from core.dimons.PrimeTest import *


class ExtendedGaluaFieldForm(QMainWindow):
	def __init__(self, *args):
		super(ExtendedGaluaFieldForm, self).__init__(*args)
		loadUi('GUI/ui/PolynomWindow.ui', self)
		self.errorLabel.setText("")
		self.qInput.setValidator(QRegExpValidator(QRegExp("\d+"), self.qInput))
		self.qInput.textChanged.connect(self.qInput_text_changed)
		self.polynomInput.textChanged.connect(self.polynomInput_text_changed)
		self.firstOperandLineEdit.textChanged.connect(self.firstOperand_text_changed)
		self.secondOperandLineEdit.textChanged.connect(self.secondOperand_text_changed)
		self.polynomInput.setValidator(QRegExpValidator(QRegExp("\d*x\^2([+\-]\d*x)?([+\-]\d+)?"), self.polynomInput))
		self.firstOperandLineEdit.setValidator(QRegExpValidator(QRegExp("(\d*x)?([+\-]\d+)?"), self.firstOperandLineEdit))
		self.secondOperandLineEdit.setValidator(QRegExpValidator(QRegExp("(\d*x)?([+\-]\d+)?"), self.secondOperandLineEdit))
		operators = [
			self.tr('+'),
			self.tr('-'),
			self.tr('*'),
			self.tr('/'),
		]
		self.operandComboBox.clear()
		self.operandComboBox.addItems(operators)

		self.operandComboBox.setEnabled(False)
		self.polynomInput.setEnabled(False)
		self.firstOperandLineEdit.setEnabled(False)
		self.secondOperandLineEdit.setEnabled(False)
		self.calculateButton.setEnabled(False)
		self.correctQ = False
		self.correctPoly = False
		self.correctFirstOperand = False
		self.correctSecondOperand = False

		self.q = None
		self.basePolynom = None
		#self.uncheckedData = True
		# self.correctP = False
		# self.correctQ = False
		# self.correctE = False
		# self.correctD = False
		# self.uncheckedData = True
		# self.extendedGcdDemo.triggered.connect(self.extendedGcdDemoClicked)
		# self.modPowDemo.triggered.connect(self.modPowDemoClicked)
		# self.primeTestDemo.triggered.connect(self.primeTestDemoClicked)
		# self.pEdit.setValidator(QRegExpValidator(QRegExp("\d+"), self.pEdit))
		# self.pEdit.textChanged.connect(self.pEdit_text_changed)
		# self.qEdit.setValidator(QRegExpValidator(QRegExp("\d+"), self.qEdit))
		# self.qEdit.textChanged.connect(self.qEdit_text_changed)
		# self.eEdit.setValidator(QRegExpValidator(QRegExp("\d+"), self.eEdit))
		# self.eEdit.textChanged.connect(self.eEdit_text_changed)
		# self.dEdit.setValidator(QRegExpValidator(QRegExp("\d+"), self.dEdit))
		# self.dEdit.textChanged.connect(self.dEdit_text_changed)
	@pyqtSlot()
	def on_calculateButton_clicked(self):
		self.qInput.setEnabled(False)
		self.operandComboBox.setEnabled(False)
		self.polynomInput.setEnabled(False)
		self.firstOperandLineEdit.setEnabled(False)
		self.secondOperandLineEdit.setEnabled(False)

		operations = {"+": addPoly, "-": difPoly, "*": mulPoly, "/": divPoly}

		_, b, c = ExtendedGaluaFieldForm.parsePolynom(self.firstOperandLineEdit.text())

		p1 = polynom2(b, c)

		_, b, c = ExtendedGaluaFieldForm.parsePolynom(self.secondOperandLineEdit.text())

		p2 = polynom2(b, c)

		self.statementOutput.setPlainText(str(operations[self.operandComboBox.currentText()](p1, p2, self.basePolynom, self.q)))


		self.qInput.setEnabled(True)
		self.operandComboBox.setEnabled(True)
		self.polynomInput.setEnabled(True)
		self.firstOperandLineEdit.setEnabled(True)
		self.secondOperandLineEdit.setEnabled(True)

	# def extendedGcdDemoClicked(self):
	#     self.extendedGcdDemoWindow = extendedGCDWindow()
	#     self.extendedGcdDemoWindow.show()
	#
	# def modPowDemoClicked(self):
	#     self.powModWindow = PowModWindow()
	#     self.powModWindow.show()
	#
	# def primeTestDemoClicked(self):
	#     self.primeTestWindow = primeTestWindow()
	#     self.primeTestWindow.show()
	#
	# @pyqtSlot()
	# def on_generateBtn_clicked(self):
	#     params = {'bits': self.keySizeBox.value()}
	#     if self.correctP is True:
	#         params['p'] = int(self.pEdit.text())
	#     else:
	#         self.correctP = True
	#         self.pLabel.setStyleSheet('color : black')
	#     if self.correctQ is True:
	#         params['q'] = int(self.qEdit.text())
	#     else:
	#         self.correctQ = True
	#         self.qLabel.setStyleSheet('color : black')
	#     if self.correctE is True:
	#         params['e'] = int(self.eEdit.text())
	#     else:
	#         self.correctE = True
	#         self.eLabel.setStyleSheet('color : black')
	#     if self.correctD is True:
	#         params['d'] = int(self.dEdit.text())
	#     else:
	#         self.correctD = True
	#         self.dLabel.setStyleSheet('color : black')
	#     start = datetime.datetime.now()
	#     p, q, n, fi, e, d = generateRsaParams(**params)
	#     finish = datetime.datetime.now()
	#     self.uncheckedData = False
	#     self.generateInfoLabel.setText(str(finish-start))
	#     self.pEdit.setText(str(p))
	#     self.qEdit.setText(str(q))
	#     self.nEdit.setText(str(n))
	#     self.fiEdit.setText(str(fi))
	#     self.eEdit.setText(str(e))
	#     self.dEdit.setText(str(d))
	#     self.correctP = True
	#     self.correctQ = True
	#     self.correctE = True
	#     self.correctD = True
	#     self.uncheckedData = True
	#
	# @pyqtSlot()
	# def on_codeBtn_clicked(self):
	#     self.on_codeClearBtn_clicked()
	#     if self.correctP and self.correctQ and self.correctE:
	#         message = self.messageEdit.text()
	#         e = int(self.eEdit.text())
	#         n = int(self.nEdit.text())
	#
	#         start = datetime.datetime.now()
	#         codes = codeRSA(message, e, n)
	#         finish = datetime.datetime.now()
	#         self.codingInfoLabel.setText(str(finish - start))
	#
	#         codedMessage = ""
	#         for code in codes:
	#             codedMessage += str(code) + ' '
	#         self.codeEdit.setText(codedMessage)
	#
	# @pyqtSlot()
	# def on_decodeBtn_clicked(self):
	#     self.on_messageClearBtn_clicked()
	#     if self.correctP and self.correctQ and self.correctD:
	#         codedMessage = self.codeEdit.text().split(' ')
	#         codes = []
	#         for c in codedMessage:
	#             try:
	#                 codes.append(int(c))
	#             except ValueError:
	#                 None
	#         d = int(self.dEdit.text())
	#         n = int(self.nEdit.text())
	#         start = datetime.datetime.now()
	#         decodedMessage = decodeRSA(codes, d, n)
	#         finish = datetime.datetime.now()
	#         self.codingInfoLabel.setText(str(finish - start))
	#         self.messageEdit.setText(decodedMessage)
	#
	# @pyqtSlot()
	# def on_messageClearBtn_clicked(self):
	#     self.messageEdit.setText("")
	#
	# @pyqtSlot()
	# def on_codeClearBtn_clicked(self):
	#     self.codeEdit.setText("")
	#
	def qInput_text_changed(self):
		if self.qInput.text() != '':
			q = int(self.qInput.text())
			if primeTest(q) is True:
				self.qLabel.setStyleSheet('color : green')
				self.q = q
				self.correctQ = True
			else:
				self.qLabel.setStyleSheet('color : red')
				self.correctQ = False
		else:
			self.qLabel.setStyleSheet('color : black')
			self.correctQ = False

		if self.correctQ is True:
			self.polynomInput.setEnabled(True)
		else:
			self.polynomInput.setText("")
			self.polynomInput.setEnabled(False)
			self.q = None

	@staticmethod
	def parsePolynom(poly):
		a, b, c = 0, 0, 0

		def getRatio(exp):
			if len(exp) == 0:
				return 1
			elif len(exp) == 1 and exp[0] == '-':
				return -1
			else:
				return int(exp)

		square_ratio = re.search(r"-?\d*x\^2", poly)
		if square_ratio is not None:
			a = getRatio(square_ratio[0][:-3])
			poly = poly[len(square_ratio[0]):]
		line_ratio = re.search(r"[\-+]?\d*x", poly)
		if line_ratio is not None:
			b = getRatio(line_ratio[0][:-1])
			poly = poly[len(line_ratio[0]):]
		if len(poly) > 0:
			c = int(poly)
		return a, b, c

	def polynomInput_text_changed(self):
		if self.polynomInput.text() != '' and re.fullmatch(r"\d*x\^2([+\-]\d*x)?([+\-]\d+)?", self.polynomInput.text()) is not None:
			a, b, c = ExtendedGaluaFieldForm.parsePolynom(self.polynomInput.text())
			poly = polynom3(a, b, c)
			if checkIrreduciblePolynomial(poly, self.q) is True:
				self.polynomLabel.setStyleSheet('color : green')
				b = (self.q - b) % self.q
				c = (self.q - c) % self.q
				self.basePolynom = polynom2(div(b, a, self.q), div(c, a, self.q))
				self.correctPoly = True
			else:
				self.polynomLabel.setStyleSheet('color : red')
				self.correctPoly = False
		else:
			self.polynomLabel.setStyleSheet('color : black')
			self.correctPoly = False

		if self.correctPoly is True:
			self.firstOperandLineEdit.setEnabled(True)
			self.secondOperandLineEdit.setEnabled(True)
		else:
			self.firstOperandLineEdit.setText("")
			self.secondOperandLineEdit.setText("")
			self.firstOperandLineEdit.setEnabled(False)
			self.secondOperandLineEdit.setEnabled(False)
			self.basePolynom = None

	def firstOperand_text_changed(self):
		if self.firstOperandLineEdit.text() != '' and re.fullmatch(r"(\d*x)?([+\-]\d+)?", self.firstOperandLineEdit.text()) is not None:
			self.correctFirstOperand = True
		else:
			self.correctFirstOperand = False

		if self.correctFirstOperand and self.correctSecondOperand:
			self.operandComboBox.setEnabled(True)
			self.calculateButton.setEnabled(True)
		else:
			self.operandComboBox.setEnabled(False)
			self.calculateButton.setEnabled(False)


	def secondOperand_text_changed(self):
		if self.secondOperandLineEdit.text() != '' and re.fullmatch(r"(\d*x)?([+\-]\d+)?", self.secondOperandLineEdit.text()) is not None:
			self.correctSecondOperand = True
		else:
			self.correctSecondOperand = False

		if self.correctFirstOperand and self.correctSecondOperand:
			self.operandComboBox.setEnabled(True)
			self.calculateButton.setEnabled(True)
		else:
			self.operandComboBox.setEnabled(False)
			self.calculateButton.setEnabled(False)
	#
	# def qEdit_text_changed(self):
	#     if self.uncheckedData is True:
	#         if self.qEdit.text() != '':
	#             q = int(self.qEdit.text())
	#             if primeTest(q) is True:
	#                 self.qLabel.setStyleSheet('color : green')
	#                 self.correctQ = True
	#             else:
	#                 self.qLabel.setStyleSheet('color : red')
	#                 self.correctQ = False
	#             self.n_fi_Edit_text_changed()
	#         else:
	#             self.nEdit.setText('')
	#             self.fiEdit.setText('')
	#             self.qLabel.setStyleSheet('color : black')
	#             self.correctQ = False
	#
	# def n_fi_Edit_text_changed(self):
	#     if self.uncheckedData is True:
	#         if self.correctP and self.correctQ:
	#             p = int(self.pEdit.text())
	#             q = int(self.qEdit.text())
	#             self.nEdit.setText(str(p*q))
	#             self.fiEdit.setText(str((p - 1) * (q - 1)))
	#         else:
	#             self.nEdit.setText('')
	#             self.fiEdit.setText('')
	#         self.eEdit_text_changed()
	#
	# def eEdit_text_changed(self):
	#     if self.uncheckedData is True:
	#         if self.correctP and self.correctQ and self.eEdit.text() != '':
	#             e = int(self.eEdit.text())
	#             fi = int(self.fiEdit.text())
	#             if gcd(e, fi) == 1:
	#                 self.eLabel.setStyleSheet('color : green')
	#                 self.correctE = True
	#             else:
	#                 self.eLabel.setStyleSheet('color : red')
	#                 self.correctE = False
	#         else:
	#             self.eLabel.setStyleSheet('color : black')
	#             self.correctE = False
	#         self.dEdit_text_changed()
	#
	# def dEdit_text_changed(self):
	#     if self.uncheckedData is True:
	#         if self.correctE and self.dEdit.text() != '':
	#             e = int(self.eEdit.text())
	#             fi = int(self.fiEdit.text())
	#             d = int(self.dEdit.text())
	#             if e*d % fi == 1:
	#                 self.dLabel.setStyleSheet('color : green')
	#                 self.correctD = True
	#             else:
	#                 self.dLabel.setStyleSheet('color : red')
	#                 self.correctD = False
	#         else:
	#             self.dLabel.setStyleSheet('color : black')
	#             self.correctD = False