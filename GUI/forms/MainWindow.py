from PyQt5.QtWidgets import QMainWindow

from GUI.forms.MillerRabin import MillerRabin
from GUI.forms.BruteForce import BruteForce
from GUI.view.Ui_MainWindow import Ui_MainWindow


class MainWindow(QMainWindow, Ui_MainWindow):

	def __init__(self):
		QMainWindow.__init__(self)
		self.setupUi(self)
		self.tabWidget.addTab(BruteForce(), BruteForce.getCaptionTab())
		self.tabWidget.addTab(MillerRabin(), MillerRabin.getCaptionTab())

	def on_tabWidget_currentChanged(self, p_int):
		self.setMaximumSize(self.tabWidget.currentWidget().size())
		print(self.tabWidget.currentWidget().size())