from PyQt5.QtWidgets import QWidget

from GUI.CustomThread import CustomThread
from GUI.utils.utils import leaveOnlyNumbers
from GUI.view.Ui_BruteForce import Ui_BruteForce
from core.Brute import is_prime


class BruteForce(QWidget, Ui_BruteForce):
	def __init__(self):
		QWidget.__init__(self)
		self.setupUi(self)

	@staticmethod
	def getCaptionTab():
		return "Метод перебора"

	def on_check_pushButton_clicked(self):
		self.result_label.setText("")
		n = int(self.n_lineEdit.text())
		def execution_function():
			return n, is_prime(n)

		def callback(result):
			number, isPrime = result
			if isPrime:
				#todo: убрать {0}
				self.result_label.setText("Число {0} является простым".format(number))
			else:
				self.result_label.setText("Число {0} НЕ является простым".format(number))

		execution_function = execution_function
		callback_function = callback
		CustomThread("BruteForce", execution_function, callback_function).start()

	def on_n_lineEdit_textChanged(self):
		number = self.n_lineEdit.text()
		try:
			number = int(number)
		except Exception:
			self.n_lineEdit.setText(leaveOnlyNumbers(self.n_lineEdit.text()))