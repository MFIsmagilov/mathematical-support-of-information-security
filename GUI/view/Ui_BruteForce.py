# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'BruteForce.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_BruteForce(object):
    def setupUi(self, BruteForce):
        BruteForce.setObjectName("BruteForce")
        BruteForce.resize(500, 250)
        BruteForce.setMinimumSize(QtCore.QSize(500, 250))
        BruteForce.setMaximumSize(QtCore.QSize(500, 250))
        BruteForce.setStyleSheet("")
        self.verticalLayout = QtWidgets.QVBoxLayout(BruteForce)
        self.verticalLayout.setContentsMargins(35, 10, 35, 10)
        self.verticalLayout.setObjectName("verticalLayout")
        self.frame = QtWidgets.QFrame(BruteForce)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.frame)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.task_label = QtWidgets.QLabel(self.frame)
        self.task_label.setObjectName("task_label")
        self.horizontalLayout.addWidget(self.task_label)
        self.verticalLayout.addWidget(self.frame)
        self.frame_2 = QtWidgets.QFrame(BruteForce)
        self.frame_2.setAutoFillBackground(False)
        self.frame_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.formLayout = QtWidgets.QFormLayout(self.frame_2)
        self.formLayout.setObjectName("formLayout")
        self.n_label = QtWidgets.QLabel(self.frame_2)
        self.n_label.setStyleSheet("QLabel{\n"
"    size: 14px;\n"
"}")
        self.n_label.setObjectName("n_label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.n_label)
        self.n_lineEdit = QtWidgets.QLineEdit(self.frame_2)
        self.n_lineEdit.setObjectName("n_lineEdit")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.n_lineEdit)
        self.check_pushButton = QtWidgets.QPushButton(self.frame_2)
        self.check_pushButton.setMinimumSize(QtCore.QSize(100, 0))
        self.check_pushButton.setObjectName("check_pushButton")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.check_pushButton)
        self.result_label = QtWidgets.QLabel(self.frame_2)
        self.result_label.setText("")
        self.result_label.setObjectName("result_label")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.result_label)
        self.frame.raise_()
        self.n_label.raise_()
        self.n_lineEdit.raise_()
        self.check_pushButton.raise_()
        self.result_label.raise_()
        self.verticalLayout.addWidget(self.frame_2)

        self.retranslateUi(BruteForce)
        QtCore.QMetaObject.connectSlotsByName(BruteForce)

    def retranslateUi(self, BruteForce):
        _translate = QtCore.QCoreApplication.translate
        BruteForce.setWindowTitle(_translate("BruteForce", "Form"))
        self.task_label.setText(_translate("BruteForce", "<html><head/><body><p align=\"justify\"><span style=\" font-size:10pt; font-weight:600;\">Функция провeки простоты натурального</span></p><p align=\"justify\"><span style=\" font-size:10pt; font-weight:600;\">числа решетом Эратосфена.</span></p></body></html>"))
        self.n_label.setText(_translate("BruteForce", "N"))
        self.check_pushButton.setText(_translate("BruteForce", "Проверить"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    BruteForce = QtWidgets.QWidget()
    ui = Ui_BruteForce()
    ui.setupUi(BruteForce)
    BruteForce.show()
    sys.exit(app.exec_())

