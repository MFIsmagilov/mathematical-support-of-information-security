# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'MainWindow.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(662, 582)
        MainWindow.setStyleSheet("QPushButton {\n"
"    background-color:#ffec64;\n"
"    border-radius:4px;\n"
"    border:1px solid #ffaa22;\n"
"    color:#333333;\n"
"    font-family:\"Segoe UI\";\n"
"    font-size:14px;\n"
"    height:30px;\n"
"    line-height:30px;\n"
"    width:90px;\n"
"    text-decoration:none;\n"
"    text-align:center;\n"
"}\n"
"QPushButton:disabled{\n"
"    background-color:#cb881b;\n"
"    border:1px solid #bd9009;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"    background-color:#ffab23;\n"
"}\n"
"\n"
"QGroupBox{\n"
"    font-family:\"Segoe UI\";\n"
"    font-size:16px;\n"
"}\n"
"QLineEdit{\n"
"    font-family:\"Segoe UI\";\n"
"}\n"
"\n"
"QLabel{\n"
"    font-family:\"Segoe UI\";\n"
"    font-size:14px;\n"
"}s")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setEnabled(True)
        self.tabWidget.setObjectName("tabWidget")
        self.gridLayout.addWidget(self.tabWidget, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(-1)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "МОЗИ"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

