# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'MillerRabin.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MillerRabin(object):
    def setupUi(self, MillerRabin):
        MillerRabin.setObjectName("MillerRabin")
        MillerRabin.resize(500, 250)
        MillerRabin.setMinimumSize(QtCore.QSize(500, 250))
        MillerRabin.setMaximumSize(QtCore.QSize(500, 250))
        MillerRabin.setStyleSheet("")
        self.verticalLayout = QtWidgets.QVBoxLayout(MillerRabin)
        self.verticalLayout.setContentsMargins(35, 10, 35, 10)
        self.verticalLayout.setObjectName("verticalLayout")
        self.frame = QtWidgets.QFrame(MillerRabin)
        self.frame.setMinimumSize(QtCore.QSize(0, 0))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.frame)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.task_label = QtWidgets.QLabel(self.frame)
        self.task_label.setObjectName("task_label")
        self.horizontalLayout.addWidget(self.task_label)
        self.verticalLayout.addWidget(self.frame)
        self.frame_2 = QtWidgets.QFrame(MillerRabin)
        self.frame_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.formLayout = QtWidgets.QFormLayout(self.frame_2)
        self.formLayout.setObjectName("formLayout")
        self.n_label = QtWidgets.QLabel(self.frame_2)
        self.n_label.setStyleSheet("QLabel{\n"
"    size: 14px;\n"
"}")
        self.n_label.setObjectName("n_label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.n_label)
        self.n_lineEdit = QtWidgets.QLineEdit(self.frame_2)
        self.n_lineEdit.setObjectName("n_lineEdit")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.n_lineEdit)
        self.k_label = QtWidgets.QLabel(self.frame_2)
        self.k_label.setObjectName("k_label")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.k_label)
        self.k_lineEdit = QtWidgets.QLineEdit(self.frame_2)
        self.k_lineEdit.setObjectName("k_lineEdit")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.k_lineEdit)
        self.check_pushButton = QtWidgets.QPushButton(self.frame_2)
        self.check_pushButton.setObjectName("check_pushButton")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.check_pushButton)
        self.result_label = QtWidgets.QLabel(self.frame_2)
        self.result_label.setText("")
        self.result_label.setObjectName("result_label")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.result_label)
        self.verticalLayout.addWidget(self.frame_2)

        self.retranslateUi(MillerRabin)
        QtCore.QMetaObject.connectSlotsByName(MillerRabin)

    def retranslateUi(self, MillerRabin):
        _translate = QtCore.QCoreApplication.translate
        MillerRabin.setWindowTitle(_translate("MillerRabin", "Form"))
        self.task_label.setText(_translate("MillerRabin", "<html><head/><body><p align=\"justify\"><span style=\" font-size:10pt; font-weight:600;\">Функция проверки простоты натурального </span></p><p align=\"justify\"><span style=\" font-size:10pt; font-weight:600;\">числа алгоритмом Миллера-Рабина.</span></p></body></html>"))
        self.n_label.setText(_translate("MillerRabin", "N"))
        self.k_label.setText(_translate("MillerRabin", "K"))
        self.check_pushButton.setText(_translate("MillerRabin", "Проверить"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MillerRabin = QtWidgets.QWidget()
    ui = Ui_MillerRabin()
    ui.setupUi(MillerRabin)
    MillerRabin.show()
    sys.exit(app.exec_())

