from threading import Thread


class CustomThread(Thread):

	def __init__(self, name, execution_function, callback_function):
		Thread.__init__(self)
		self.name = name
		self.execution_function = execution_function
		self.callback_function = callback_function

	def run(self):
		self.callback_function(self.execution_function())
