import re

def isNumber(symbol):
	return symbol >= '0' and symbol <= '9'


def leaveOnlyNumbers(text):
	return re.sub(r"[^0-9]", "", text)

print(leaveOnlyNumbers("55емир345"))