import math

def erythrophene_sieve(n):
	b = [True] * (n + 1)
	ps = []
	for p in range(2, n + 1):
		if b[p]:
			ps.append(p)
			for i in range(p, n + 1, p):
				b[i] = False
	return ps

def is_prime(n):
	if n < 2: return False

	if n & 1 == 0: return False

	k = 2
	while k*k < n + 1:
		if n % k == 0:
			return False
		k += 1
	return True


print(is_prime(25))