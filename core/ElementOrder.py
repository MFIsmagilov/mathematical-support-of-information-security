def ord(a, p):
	for t in range(1, p):
		if a ** t % p == 1:
			return t


p = 11

for a in range(1, p):
	print("{0:>4}".format(str(a) + " "), end = '')
print()

generators = []
for a in range(1, p):
	elem = ord(a, p)
	print("{0:>4}".format(str(elem) + " "), end = '')
	if elem == p - 1:
		generators.append(a)
print("\nПримитивные элементы поля F{0}: ".format(p), end = '')
print(generators)
print()


