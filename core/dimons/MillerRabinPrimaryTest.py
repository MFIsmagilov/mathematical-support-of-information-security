from .StrongPseudoprimeNumbers import *
from .PrimeNumbers import *
from random import randint
from math import log10, log2

def millerRabinTest(p):

    d = p - 1
    while d & 1 != 1:
        d >>= 1

    for k in strongPseudoprimeNumbers.keys():
        if p < k:
            for i in range(1, strongPseudoprimeNumbers[k]):
                if millerRabinSingleTest(p, d, firstPrimeNumbers[i]) is False:
                    return False
            else:
                return True
    else:
        for i in range(0, int(log2(p))):
            if fermatPrimaryTest(p) is False:
                return False
        else:
            for i in range(1, int(log10(p))):
                if millerRabinSingleTest(p, d, firstPrimeNumbers[randint(0, len(firstPrimeNumbers)-1)]) is False:
                    return False
            else:
                return True

def fermatPrimaryTest(p):
    a = randint(2, p)
    if pow(a, p-1, p) == 1:
        return True
    else:
        return False


def millerRabinSingleTest(p, d, a):

    if p & 1 == 0:
        return False

    a = a if a < p else a % p

    remainder = pow(a, d, p)

    if remainder == 1:
        return True
    else:
        while d < p-1 and remainder != p-1:
            d *= 2
            remainder = pow(remainder, 2, p)

        return True if (remainder == p-1) else False