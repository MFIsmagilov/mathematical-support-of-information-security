def inv(a, module):
	for n in range(1,module):
		if (a * n) % module == 1:
			return n

def div(a, b, module):
	return (a * inv(b, module)) % module

def addPoly(p1, p2, base, module):
	return polynom2((p1.a + p2.a) % module, (p1.b + p2.b) % module)

def difPoly(p1, p2, base, module):
	return polynom2((p1.a - p2.a) % module, (p1.b - p2.b) % module)

def mulPoly(p1, p2, base, module):
	return polynom2((p1.a*p2.b + p1.b*p2.a + p1.a*p2.a*base.a) % module, (p1.b*p2.b + p1.a*p2.a*base.b) % module)

def divPoly(p1, p2, base, module):
	pass

class polynom3:
	def __init__(self, a, b, c):
		self.a = a
		self.b = b
		self.c = c

	def evaluate(self, x, module):
		return (self.a*x*x + self.b*x + self.c) % module

def checkIrreduciblePolynomial(p3, module):
	for n in range(0, module):
		if p3.evaluate(n, module) == 0:
			return False
	return True



class polynom2:
	def __init__(self, a, b):
		self.a = a
		self.b = b

	def __str__(self):
		res = ""
		if self.a != 0:
			if self.a != 1:
				res += str(self.a)
			res += "x"
		if self.b != 0:
			if len(res) > 0 and self.b >= 0:
				res += "+"
			res += str(self.b)
		else:
			if len(res) == 0:
				res = "0"
		return res

	def evaluate(self, x, module):
		return (self.a * x + self.b)%module
