from .PrimeNumbers import firstPrimeNumbers
from .MillerRabinPrimaryTest import millerRabinTest

def primeTest(p):
    if p < firstPrimeNumbers[len(firstPrimeNumbers) - 1]:
        return firstPrimeNumbers.__contains__(p)
    for prime in firstPrimeNumbers:
        if p % prime is 0:
            return False
    else:
        return millerRabinTest(p)