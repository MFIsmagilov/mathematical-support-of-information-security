import random
# n - проверяемое число
# k - количество раундов
def testMillerRabin(n, k):
	if n < 2:
		return False
	if n == 2 or n == 3:
		return True
	if n & 1 == 0:  # % 2
		return False
	r = 0
	d = n - 1

	while d & 1 == 0:
		d = d >> 1
		r += 1

	for _ in range(k):
		a = random.randint(2, n - 2)
		x = pow(a, d, n)

		if x == 1 or x == n - 1:
			continue

		for _ in range(r - 1):

			x = pow(x, 2, n)
			if x == 1:
				return False
			if x == n - 1:
				break
		if x != n - 1:
			return False

	return True
